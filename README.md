# Webpack + Angular.js starter kit
## Installation
```
#!js
npm install
```

## Commandes

### Démarrer le serveur de développment
```
#!js
npm run dev
```

### Compiler les assets pour la mise en production
```
#!js
npm run build
```

## Repertoires
Les repertoires de bases sont:

build : contient les scripts node et la configuration webpack

src : contient les fichiers sources de l'application web

Le fichier src/index.html contient le squelette de l'application.
Dans la balise body se trouve une balise app qui sera remplacée par le root component angular.

## Fonctionnement de webpack dans ce workflow

Webpack est un outil plein de ressources. Dans ce build il est mis en oeuvre avec une configuration orientée development et une autre orientée production.

Les fichiers de configuration se trouvent dans le repertoire build.