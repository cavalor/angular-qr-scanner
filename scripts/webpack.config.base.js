const config           = require('../config');
const ngAnnotatePlugin = require('ng-annotate-webpack-plugin');
const autoprefixer     = require('autoprefixer');
const path             = require('path');

module.exports = {
  entry: config.entry,
  output: {
    path: config.build.path,
    publicPath: config.build.publicDirectory,
    filename: '[name].js'
  },
  resolve: {
    extensions: ['', '.js', '.coffee', '.css', '.scss', '.sass', '.styl', '.html'],
    fallback: [
      path.join(config.projectRoot, 'node_modules'),
      path.join(config.projectRoot, 'web_modules')
    ],
    alias: {
      'app': path.join(config.projectRoot, 'src/app'),
      'components': path.join(config.projectRoot, 'src/app/components'),
      'style': path.join(config.projectRoot, 'src/style'),
      'image': path.join(config.projectRoot, 'src/static/image')
    }
  },
  module: {
    preLoaders: [
      {
        test: /\.js$/,
        loader: 'eslint',
        include: config.projectRoot,
        exclude: /node_modules/
      }
    ],
    loaders: [
      {
        test: /\.html$/,
        loader: 'html'
      },
      {
        test: /\.(sass|scss)$/,
        loaders: ['style', 'css', 'postcss', 'sass'],
        type: 'style'
      },
      {
        test: /\.styl$/,
        loaders: ['style', 'css', 'postcss', 'stylus'],
        type: 'style'
      },
      {
        test: /\.css$/,
        loaders: ['style', 'css', 'postcss'],
        type: 'style'
      },
      {
        test: /\.coffee$/,
        loader: 'coffee',
        exclude: /node_modules/
      },
      {
        test: /\.js$/,
        loader: 'babel',
        include: config.projectRoot,
        exclude: /node_modules|libs/
      },
      {
        test: /\.(png|jpg|gif|svg|woff2?|eot|ttf)(\?.*)?$/,
        loader: 'url',
        query: {
          limit: 10000,
          name: '[name].[ext]'
        }
      }
    ]
  },
  sassLoader: {
    includePaths: [
      path.resolve(config.projectRoot, 'src/style')
    ]
  },
  plugins: [
    new ngAnnotatePlugin({
        add: true
    })
  ],
  eslint: {
    configFile: config.projectRoot + '/.eslintrc',
    formatter: require('eslint-friendly-formatter')
  },
  postcss: () => {
      return [autoprefixer(config.autoprefixer)];
  }
}
