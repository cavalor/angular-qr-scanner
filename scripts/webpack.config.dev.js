const webpack = require('webpack');
const config = require('../config');
const webpackConfig = require('./webpack.config.base');
const HtmlWebpackPlugin = require('html-webpack-plugin');

// add hot-reload related code to entry chunks
Object.keys(webpackConfig.entry).forEach(function (name) {
  webpackConfig.entry[name] = ['./scripts/dev-client'].concat(webpackConfig.entry[name]);
});

webpackConfig.devtool = '#eval-source-map';
webpackConfig.plugins = webpackConfig.plugins.concat([
  new webpack.optimize.OccurenceOrderPlugin(),
  new webpack.HotModuleReplacementPlugin(),
  new webpack.NoErrorsPlugin(),
  new HtmlWebpackPlugin({
    filename: 'index.html',
    template: 'src/app/index.html',
    inject: true
  })
]);

module.exports = webpackConfig;
