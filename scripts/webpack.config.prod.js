const webpack           = require('webpack');
const webpackConfig     = require('./webpack.config.base');
const ora               = require('ora');
const extractTextPlugin = require('extract-text-webpack-plugin');
const extractStylesheet = new extractTextPlugin('app.css');
const HtmlWebpackPlugin = require('html-webpack-plugin');

webpackConfig.plugins = webpackConfig.plugins.concat([
  extractStylesheet,
  new webpack.optimize.UglifyJsPlugin({
    comments: false,
    compress: {
      warnings: false
    }
  }),
  new webpack.optimize.OccurenceOrderPlugin(),
  new HtmlWebpackPlugin({
    filename: 'index.html',
    template: 'src/app/index.html',
    inject: true
  })
]);

for (var key in webpackConfig.module.loaders) {
  if (webpackConfig.module.loaders[key].hasOwnProperty('type')) {
    if (webpackConfig.module.loaders[key].type === 'style') {
      var loaders = webpackConfig.module.loaders[key].loaders;
      webpackConfig.module.loaders[key].loaders = null;
      webpackConfig.module.loaders[key].loader = extractStylesheet.extract(loaders.slice(1, loaders.length));
    }
  }
}

module.exports = webpackConfig;
