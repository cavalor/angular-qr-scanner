var fs                   = require('fs');
var http                 = require('http');
var https                = require('https');
var path                 = require('path');
var express              = require('express');
var webpack              = require('webpack');
var config               = require('../config');
var webpackConfig        = require('./webpack.config.dev');
var proxyMiddleware      = require('http-proxy-middleware');
var webpackDevMiddleware = require('webpack-dev-middleware');
var webpackHotMiddleware = require('webpack-hot-middleware');


/**
 * Express app
 */
var app = express();


/**
 * Certificate
 */
var privateKey  = fs.readFileSync(path.resolve('./scripts/certificate/server.key'), 'utf8');
var certificate = fs.readFileSync(path.resolve('./scripts/certificate/server.crt'), 'utf8');
var credentials = {
  key: privateKey,
  cert: certificate
};


/**
 * Webpack compiler
 */
var compiler = webpack(webpackConfig);
// force page reload when html-webpack-plugin template changes
compiler.plugin('compilation', (compilation) => {
  compilation.plugin('html-webpack-plugin-after-emit', (data, cb) => {
    hotMiddleware.publish({ action: 'reload' });
    cb();
  });
});


/**
 * Webpack hot middleware
 * enable hot-reload and state-preserving
 * compilation error display
 */
var hotMiddleware = webpackHotMiddleware(compiler);
app.use(hotMiddleware);


/**
 * Webpack dev middleware
 */
var devMiddleware = webpackDevMiddleware(compiler, {
  publicPath: webpackConfig.output.publicPath,
  contentBase: 'src',
    stats: {
      colors: true,
      hash: false,
      timings: true,
      chunks: false,
      chunkModules: false,
      modules: false
    }
});
app.use(devMiddleware);


/**
 * Proxy api requests
 * https://github.com/chimurai/http-proxy-middleware
 */
Object.keys(config.dev.proxyTable).forEach((context) => {
  var options = proxyTable[context];
  if (typeof options === 'string') {
    options = { target: options };
  }
  app.use(proxyMiddleware(context, options));
});


/**
 * Handle fallback for HTML5 history API
 */
app.use(require('connect-history-api-fallback')());


/**
 * Serve pure static assets
 */
app.use(config.build.publicDirectory, express.static(config.src.assetsPath));


/**
 * Http(s) server
 */
app.get('*', function response(req, res) {
  res.write(devMiddleware.fileSystem.readFileSync(config.build.index));
    res.end();
});
var httpServer = http.createServer(app);
var httpsServer = https.createServer(credentials, app);
httpServer.listen(config.dev.ports.http);
httpsServer.listen(config.dev.ports.https);
console.log('==================================================');
console.log('DEV SERVER ---> http://localhost:' + config.dev.ports.http);
console.log('DEV SERVER ---> https://localhost:' + config.dev.ports.https);
console.log('==================================================');
