require('shelljs/global'); // https://github.com/shelljs/shelljs
env.NODE_ENV = 'production';
const path = require('path');
const config = require('../config');
const ora = require('ora');
const webpack = require('webpack');
const webpackConfig = require('./webpack.config.prod');
const spinner = ora('building for production...');

spinner.start();

rm('-rf', config.build.path);
mkdir('-p', config.build.path);
cp('-R', 'src/static/', config.build.path);

webpack(webpackConfig, function (err, stats) {
  spinner.stop();
  if (err) {
    throw err;
  }
  process.stdout.write(stats.toString({
    colors: true,
    modules: false,
    children: false,
    chunks: false,
    chunkModules: false
  }) + '\n');
});
