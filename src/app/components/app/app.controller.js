class AppController {

  constructor($rootScope) {
    this.menuIsOpen = false;
    $rootScope.$watch(() => {
      return this.menuIsOpen;
    }, () => {
      console.log(this.menuIsOpen);
    });
  }
  
}

export default AppController;
