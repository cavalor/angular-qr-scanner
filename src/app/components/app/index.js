import controller from './app.controller';
import template from './app.html';
import './app.scss';

const MODULE = 'rootComponent';

angular.module(MODULE, [])
.component('app', {
  controller: controller,
  controllerAs: 'app',
  template: template,
});

export default MODULE;
