// components
import app from './app';
import loadingIndicator from './loadingIndicator';
import qrScanner from './qrScanner';

export default angular.module('components', [
  app,
  loadingIndicator,
  qrScanner
]).name;
