import loadingIndicator from './loadingIndicator.directive';
import './loadingIndicator.scss';

const MODULE = 'loadingIndicator';

angular.module(MODULE, [])
  .directive('loadingIndicator', loadingIndicator);

export default MODULE;
