class MediaDevice {

  /**
   * Constructeur
   * @param  {$q} $q Service promise angular
   */
  constructor($q) {
    this.$q = $q;
    this.BACK_CAMERA = 'back';
    this.FRONT_CAMERA = 'front';
  }

  /**
   * Detection des peripheriques
   * @return {Promise}
   */
  detectVideosInputs() {

    var defer = this.$q.defer();

    // Récuperation des périphériques
    navigator.mediaDevices.enumerateDevices()
    .then((devices) => {

      var videoInputs = [];

      devices.forEach((device) => {
        // si le périphérique est une caméra on l'ajoute au tableau
        if (device.kind === 'videoinput') {
          videoInputs.push(device);
        }
      });

      // si le tableau est vide on rejette la promesse
      if (videoInputs.length === 0) {
        defer.reject('Video inputs unavailable on this device');
      }

      // si des cameras sont disponibles, on resou la promesse
      defer.resolve(videoInputs);

    })
    .catch(function(err) {

      defer.reject(err);
      console.error(err.name + ": " + err.message);

    });

    // on retourne la promesse
    return defer.promise;

  }

  /**
   * Selection de la source video
   * Ordre de priorite
   *   - deviceLabelContain parameter
   *   - back camera
   *   ... others
   * @return {Promise}
   */
  getVideoInput(deviceLabelContain) {

    var defer = this.$q.defer();

    // detection des periphériques disponibles
    this.detectVideosInputs().then((result) => {

      var input = null;

      // on parcours la liste des capteurs videos disponibles
      result.forEach((device) => {
        if (device.label.includes(deviceLabelContain)) {
          // si le capteur est la camera arriere on resou la promesse
          defer.resolve(device);
        } else if (device.label.includes('back') || input === null) {
          input = device;
        }
      });

      // pas de capteur arriere disponible, resolution avec un autre capteur
      defer.resolve(input);

    }, (error) => {

      // si une erreur survient, on la fait remonter
      defer.reject(error);

    });

    // on retourne la promesse
    return defer.promise;

  }

  /**
   * Promesse permettant de recuperer le flux video
   * @return {Promise}
   */
  getVideoStream(deviceLabelContain) {

    var defer = this.$q.defer();

    // recuperation de la fonction getUserMedia
    navigator.getUserMedia = navigator.getUserMedia ||
                             navigator.webkitGetUserMedia ||
                             navigator.mozGetUserMedia ||
                             navigator.msGetUserMedia;

    // selection de la camera par defaut
    this.getVideoInput(deviceLabelContain)
    .then((result) => {

      // contraintes parametres de la fonction
      let constraints = {
        audio: false,
        video: {
          optional: [{sourceId: result.deviceId}]
        }
      };

      // recuperation du flux
      navigator.getUserMedia(constraints, (stream) => {
        // resolution de la promesse
        defer.resolve(stream);
      }, (error) => {
        defer.reject(error);
      });

    }, (error) => {

      // en cas d'echec lors de la recuperation de la camera on fait remonter l'erreur
      defer.reject(error);

    });

    // on retourne la promesse
    return defer.promise;

  };

}

export default angular.module('services.mediaDevice', [])
  .service('MediaDevice', MediaDevice)
  .name;
