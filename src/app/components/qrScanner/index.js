import qrScanner from './qrScanner.directive';
import MediaDevicesService from './mediaDevice.service';

const MODULE = 'qrScanner';

angular.module(MODULE, [
  MediaDevicesService
])
.directive('qrScanner', qrScanner);

export default MODULE;
