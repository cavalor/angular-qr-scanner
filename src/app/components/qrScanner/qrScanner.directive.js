import jsqrcode from './lib/jsqrcode';
import template from './qrScanner.html';
import './qrScanner.scss';

/**
 * Directive permettant d'utiliser la webcam pour scanner des codes qr
 * @param  {Object} MediaDevice Service angular encapsulant les fonctionnalités webrtc
 * @param  {Object} $interval   Wrapper angular gerant les intervals
 * @return {Object} directive
 */
export default function webcamDirective(MediaDevice, $interval) {

  var postLink = function(scope, element) {

    var mediaStream, selectedDevice, snapInterval;
    var video = document.getElementById('webcam');
    var canvas = document.getElementById('canvas');
    var context = canvas.getContext('2d');

    /**
     * Fonction de callback pour la lib jsqrcode
     * On cancel l'interval de decodage quand le code est decodé
     * @param  {String} data valeur de retour de la fonction de callback
     */
    jsqrcode.callback = (data) => {
      $interval.cancel(snapInterval);
      console.log(data);
    };

    /**
     * Démarage de la webcam
     * @param  {String} device parametre optionnel permettant de specifier le periphérique a utiliser
     */
    var startWebcam = (device) => {

      selectedDevice = device || MediaDevice.BACK_CAMERA;

      MediaDevice.getVideoStream(selectedDevice)
      .then((stream) => {
        mediaStream = stream;
        video.src = window.URL.createObjectURL(stream);
        snapInterval = $interval(onSnap, 333);
      }, (err) => {
        console.error(err);
      });

    };

    /**
     * Callback apellé lors de arret de la webcam et l'event angular $destroy
     */
    var onDestroy = () => {

      if (!!mediaStream) {
        if (mediaStream.getVideoTracks && typeof mediaStream.getVideoTracks === 'function') {
          var tracks = mediaStream.getVideoTracks();
          if (tracks && tracks[0] && tracks[0].stop) {
            tracks[0].stop();
          }
        } else if (mediaStream.stop) {
          // deprecated, may be removed in the near future
          mediaStream.stop();
        }
      }

      if (!!video) {
        video.src = null;
      }

    };

    /**
     * Callback associée a l'interval permettant de lancer le decodage
     */
    var onSnap = () => {

      // Calcul de l'échelle de l'image
      var scale = window.innerWidth / video.videoWidth;

      // Aplication de l'echelle
      canvas.setAttribute('width', video.videoWidth * scale);
      canvas.setAttribute('height', video.videoHeight * scale);

      // Dessin de l'image
      context.drawImage(video, 0, 0, video.videoWidth * scale, video.videoHeight * scale);

      // Appel de la librairie jsqrcode afin de decoder l'image
      try {
        jsqrcode.decode();
      } catch(err) {
        console.error(err);
      }
    };

    scope.$on('$destroy', onDestroy);
    scope.$on('START_WEBCAM', startWebcam);
    scope.$on('STOP_WEBCAM', onDestroy);

    startWebcam();

  };

  return {
    template: template,
    restrict: 'E',
    replace: true,
    transclude: true,
    scope: {},
    link: postLink
  };

};
