// components
import components from './components/components';
// style
import 'normalize.css';

(function(){

  var config = function($locationProvider, $routeProvider, $httpProvider, $touchProvider) {
    $locationProvider.html5Mode(true);
    delete $httpProvider.defaults.headers.common["X-Requested-With"];
    $routeProvider
      .when('/qrcode', { template: '<qr-scanner></qr-scanner>'})
      .otherwise({ redirectTo: '/' });
    $touchProvider.ngClickOverrideEnabled(true);
  };

  angular.module('client', [
    'ngRoute',
    'ngTouch',
    components,
  ])
  .config(config);

})();
