const path = require('path');
const root = path.resolve(__dirname);

module.exports = {
  // root path
  projectRoot: root,
  // webpack entry points for base configuration
  entry: {
    app: [
      'angular',
      'angular-route',
      'angular-touch',
      path.resolve(root, 'src/app/index.js')
    ]
  },
  // autoprefixer configuration
  autoprefixer: { browsers: ['last 2 versions'] },
  // src path and html skelleton location
  src: {
    assetsPath: path.resolve('src/static'),
    path: path.join(root, 'src'),
    index: path.resolve(root, 'src/app/index.html'),
  },
  // output paths and options
  build: {
    path: path.resolve(root, 'public'),
    index: path.resolve(root, 'public/index.html'),
    publicDirectory: '/',
    productionSourceMap: true,
  },
  // development server options
  dev: {
    ports: {
      http: 8080,
      https: 3000
    },
    proxyTable: {}
  }
};
